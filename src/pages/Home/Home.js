import React,{useState,useEffect} from "react";
import MovieCard from "../../components/MovieCard/MovieCard";

import axios from "axios";


function Home(props){
    const [movies,setMovies]=useState([])

  useEffect(()=>{
    axios.get('http://localhost:3000/movies')
    .then((data)=>{
      console.log(data.data)
      setMovies(data.data)
    })
    .catch((error)=>{
      console.log(error)
    })
  })

    return(
        <main> 
          <section>
            <h2>Recommended Movies</h2>
               <ul className='movies'>
                {
                  movies.map((movie, index)=>
                  <MovieCard movie={movie}/>
          
                 )
                }
        
              </ul>
          </section>
       </main>
    );
}
export default Home