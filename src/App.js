import logo from './logo.svg';
import './App.css';
import Home from './pages/Home/Home';
import Header from './components/Header/Header';
import { useEffect, useState } from 'react';
import movieCard from './components/MovieCard/MovieCard';
import MovieCard from './components/MovieCard/MovieCard';
import Error from './pages/Error/Error';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import axios from 'axios';
import Movie from './pages/Movie/Movie';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home/>,
    errorElement: <Error/>,
    children: [
      {
        path:"/movie/:id",
        element:<Movie/>
      },
    ],
  },
  
]);

function App() {
  
  return (
    <>
    <Header/>
    <RouterProvider router={router} />
    
    <footer></footer>
    </>
  );
}

export default App;
