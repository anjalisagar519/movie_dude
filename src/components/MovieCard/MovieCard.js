import React from "react"

function MovieCard(props){
    const movie =(props.movie)
    return(
        <a href={"/movie/" +movie._id}>
          <li className='movie'>
          <img src={movie.image}/>
          <h3>{movie.title}</h3>
          <span>{movie.category}</span>
        </li>
    
      </a>
    )
}
export default MovieCard