function Header(){
    return(
        <header>
      <div className='logo'>
        <div className='logoCircle'>&nbsp;</div>
         <span>Movie Dude</span>
     </div>
     <nav>
      <ul>
        <li>
          <a href='#'>Home</a>
        </li>
        <li>
          <a href='#'>About</a>
        </li>
        <li>
          <a href='#'>Movies</a>
        </li>
        <li>
          <a href='#'>contact</a>
        </li>
      </ul>
     </nav>
    </header>
    )
}
export default Header